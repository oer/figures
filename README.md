<!--- Local IspellDict: en -->

This project hosts a collection of figures (images, sketches, photos,
animations) whose licenses permit use as and in
[Open Educational Resources (OER)](https://en.wikipedia.org/wiki/Open_educational_resources).
Briefly, OER are media for teaching and learning—such as articles,
books, figures, presentations, videos, games, simulations—whose
license terms permit the
[5 Rs: Retain, Reuse, Revise, Remix, Redistribute](http://opencontent.org/blog/archives/3221).
Mostly, [Creative Commons (CC) licenses](https://en.wikipedia.org/wiki/Creative_Commons_license)
are used for OER.  Note that in my view, which is authoritative here,
licenses for OER *must* allow commercial use.  Thus, I will not
include figures with a [non-commercial CC license](https://en.wikipedia.org/wiki/Creative_Commons_license#Non-commercial_licenses).
However, be warned that I publish meta-data for figures, excluding the
figures themselves, if I am permitted to use them for my purposes even
if they do not qualify as OER; I embed such figures from their origin
servers with [emacs-reveal](https://gitlab.com/oer/emacs-reveal).
The fact that I am allowed to do this does not imply anything about
your rights, see [there for some copyright aspects](https://en.wikipedia.org/wiki/Copyright_aspects_of_hyperlinking_and_framing#Decision_I-20_U_42/11_Dusseldorf_Court_of_Appeal_8_October_2011).

If you ever created OER, you are probably aware that proper
attribution can be a pain, in particular as most image formats do not
provide any means to embed license information.  Thus, even when
copying figures between my own presentations, I repeatedly had to copy
and paste license information, which is an idiotic task.  To simplify
reuse of OER figures, this project publishes two files for each
figure, (1) the figure itself and (2) a meta-data file with licensing
information.  The format of that meta-data file is understood by the
[free](https://en.wikipedia.org/wiki/Free_software) software
[emacs-reveal](https://gitlab.com/oer/emacs-reveal) to display the
figure with proper attribution in HTML presentations based on the HTML
framework [reveal.js](https://revealjs.com/).  If you agree that such
a structured exchange of license meta-data is a Good Thing and if you
would like to contribute, feel free to contact me.  I believe that
this project might be valuable for anyone working with images in OER,
regardless of what OER are produced how.  If you want to support a
different target environment or disagree with my decisions, please
contact me as well.  So far, I’m the only user of this project, so
changes can be performed rather easily.

For the definition of meta-data, I use a pragmatic approach based on
accepted terminologies
([Dublin Core](https://en.wikipedia.org/wiki/Dublin_Core) and
[Creative Commons](https://wiki.creativecommons.org/wiki/RDFa))
as described in [this paper](https://dl.gi.de/handle/20.500.12116/24399).
For example, the
[meta-data file](https://gitlab.com/oer/figures/blob/master/3d-man/decision.meta)
for the [sample image](https://gitlab.com/oer/figures/blob/master/3d-man/decision-1013751_1920.jpg)
at the bottom of this README looks as follows and contains some
explanatory comments:

```lisp
;; Semicolon starts comment until end of line (Emacs Lisp).
;; Note that the line for dc:title below is just a comment.  In that
;; case, "Image" is used as generic title; uncomment for real title.
;; CC0 does not require attribution of author/creator; uncomment if needed.

((filename . "./figures/3d-man/decision-1013751_1920.jpg") ; Note the path prefix
; (dc:title . "The title given by the author")
 (licenseurl . "https://creativecommons.org/publicdomain/zero/1.0/")
 (licensetext . "CC0")
; (cc:attributionName . "Jens Lechtenbörger")
; (cc:attributionURL . "https://gitlab.com/lechten")
 (dc:source . "https://pixabay.com/en/decision-question-response-1013751/")
 (sourcetext . "Pixabay")
 (imgalt . "Balance tipping in favor of Yes")
 (imgadapted . "converted from") ; Adjust as needed
 (texwidth . 0.5) ; Width in percent of textwidth for LaTeX export
)
```

Further attributes not shown above include:
- copyright: Copyright information that should be reproduced
- dcmitype: Default is StillImage
- dependencies: Files that should be published by oer-reveal along
  with the current one (for SVG images that embed other images)
- permit: Special permissions

[Emacs-reveal](https://gitlab.com/oer/emacs-reveal) provides macros
(defined towards the bottom of
[https://gitlab.com/oer/emacs-reveal/blob/master/config.org](config.org))
to display figures, i.e., images along with license information,
optionally with captions.  Notably, license information is embedded using
[RDFa](https://wiki.creativecommons.org/wiki/RDFa) in HTML, making it
accessible on the
[Semantic Web](https://en.wikipedia.org/wiki/Semantic_Web).
(For example, the Firefox plugin
[OpenLink Structured Data Sniffer](https://addons.mozilla.org/en-US/firefox/addon/openlink-structured-data-sniff/)
can parse and display such RDFa data.)

See my
[howto for emacs-reveal](https://oer.gitlab.io/emacs-reveal-howto/howto.html)
([source code](https://gitlab.com/oer/emacs-reveal-howto))
for sample results and usage instructions concerning emacs-reveal in
general and figures of this repository in particular.

The repository “oer/figures” by Jens Lechtenbörger is
published under the Creative Commons license
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
Individual figures are published under their own license terms as
indicated in the meta-data file for each figure.

As mentioned above, I would be happy to receive contributions to this
repository!  Just open an
[issue](https://gitlab.com/oer/figures/issues) or
[merge request](https://gitlab.com/oer/figures/merge_requests).
If you do not like my approach towards meta-data, please contact me as
well, maybe with an issue?  A switch to a more standard meta-data
format such as JSON-LD should not be hard.

![Balance tipping in favor of Yes](3d-man/decision-1013751_1920.jpg)
