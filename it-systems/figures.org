#+SPDX-FileCopyrightText: 2024 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+name: dot-init
#+begin_src dot :exports none :results none
    graph [
	   fontname = "Helvetica,Arial,sans-serif"
	   fontsize = 20
	   layout = dot
	   ]
    node [
	  style=filled
          fillcolor=grey95
	  shape=record
          width=3
	  pencolor="#00000044" // frames color
	  fontname="Helvetica,Arial,sans-serif"
	  ]
    edge [
	  fontname="Helvetica,Arial,sans-serif"
	  ]
#+end_src

#+name: dot-graph-hw
#+begin_src dot :exports none :results none
graph [bgcolor = "#faaf90", label = "Computer Hardware\n\n"]
#+end_src

#+name: dot-node-hw
#+begin_src dot :exports none :results none
hw [label = "{Abstract Interface\n(Machine Language,\nInstruction Set Architecture)|\nComputer Platform\n\n}"]
#+end_src

#+begin_src dot :noweb yes :file 01-computer.png :exports results :results none
  digraph Computer {
  <<dot-init>>
      subgraph cluster_hw {
          <<dot-graph-hw>>
          <<dot-node-hw>>

          chip [ label = "{Abstract Interface\n(HDL)|\nChips, Circuits\n\n}"]
          gate [ label = "{Abstract Interface\n(HDL)|\nLogic Gates\n\n(Boolean Logic\nas abstraction for \nelectrical engineering)\n\n}"]
          hw -> chip [label="  consists of"]
          chip -> gate [label="  consist of"]
      }
  }
#+end_src

#+name: dot-graph-os
#+begin_src dot :exports none :results none
graph [bgcolor = "#fcc9b5", label = "Computer with OS \n\n"]
#+end_src

#+name: dot-node-os
#+begin_src dot :exports none :results none
os [ label = "{Abstract Interface\n(Kernel API)|\nOperating System (OS)\n\n}"]
#+end_src

#+begin_src dot :noweb yes :file 02-os.png :exports results :results none
  digraph OS {
     <<dot-init>>
     subgraph cluster_os {
         <<dot-graph-os>>
         subgraph cluster_hw {
             <<dot-graph-hw>>
             <<dot-node-hw>>
         }
         <<dot-node-os>>
         apps [ label = "{Abstract Interface\n(Programming Language)|\nApplications\n\n}"]
         os -> hw [label="  manages"]
         apps -> os [label="  use OS services"]
     }
  }
#+end_src

#+name: dot-graph-vm
#+begin_src dot :exports none :results none
graph [bgcolor = "#d9e4ff", label = "Computer with Virtualization or Containerization\n\n"]
#+end_src

#+begin_src dot :noweb yes :file 03-container.png :exports results :results none
  digraph Container {
      <<dot-init>>
      subgraph cluster_container {
          <<dot-graph-vm>>
          subgraph cluster_os {
             <<dot-graph-os>>
             subgraph cluster_hw {
                 <<dot-graph-hw>>
                 <<dot-node-hw>>
             }
             <<dot-node-os>>
             os -> hw [label="  manages"]
         }
         env1 [ label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
         app1 [ label = "{Abstract Interface\n(Programming Language)|\nApplications\n\n}"]
         env2 [ label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
         app2 [ label = "{Abstract Interface\n(Programming Language)|\nApplications\n\n}"]
         env3 [ label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
         app3 [ label = "{Abstract Interface\n(Programming Language)|\nApplications\n\n}"]
         env1 -> os
         env2 -> os [label="  use OS services"]
         env3 -> os
         app1 -> env1
         app2 -> env2 [label="  rely on environment"]
         app3 -> env3
     }
  }
#+end_src

#+begin_src dot :noweb yes :file 04-cloud.png :exports results :results none
  digraph Cloud {
      <<dot-init>>
      subgraph cluster_cloud {
          graph [bgcolor = "#b3c7f7", label = "Cloud of Computers\n\n"]
          node [width=2]
          env1 [label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
          app1 [label = "{Abstract Interface\n(Programming Language)|\nApplication /\nService\n\n}"]
          app2 [label = "{Abstract Interface\n(Programming Language)|\nApplication /\nService\n\n}"]
          # app3 [label = "{Abstract Interface\n(Programming Language)|\nApplication /\nService\n\n}"]
          app4 [label = "{Abstract Interface\n(Programming Language)|\nApplication /\nService\n\n}"]
          env2 [label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
          # env3 [label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
          env4 [label = "{Abstract Interface\n(Config Files)|\nExecution Environment\n(VM, Container)\n\n}"]
          app1 -> env1
          app2 -> env2
          # app3 -> env3
          app4 -> env4

          c1 [fillcolor = "#fcc9b5", label = "Computer with OS", style="filled,dashed"]
          c2 [fillcolor = "#fcc9b5", label = "Computer with OS", style="filled,dashed"]

          env1 -> c1 [style="dashed"]
          env2 -> c1 [style="dashed"]
          # env3 -> c1 [style="dashed"]
          env4 -> c2 [style="dashed", label="  runs on (using OS services)"]
      }
  }
#+end_src
